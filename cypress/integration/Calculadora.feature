#language: pt
Funcionalidade: Calculadora

    Como usuario, desejo utilizar a Calculadora
    Para que possa conferir minhas contas 

Contexto: 
    Dado que eu acesso a Calculadora
Cenario: Calcular uma soma de valres 
    E desejo realizar uma "soma"
    Quando informar os valores "2" e "2"
    E finalizar a conta
    Entao devo obter o resultado "4"

Cenario: Calcular uma subtração de valres
    E desejo realizar uma "subtração"
    Quando informar os valores "8" e "2"
    E finalizar a conta
    Entao devo obter o resultado "6"

Esquema do Cenario: Calcular valres 
    E desejo realizar uma "<operação>"
    Quando informar os valores "<valor1>" e "<valor2>"
    E finalizar a conta
    Entao devo obter o resultado "<resultado>"

    Exemplos:
    | operação        |valor1 |valor2|resultado| 
    | soma            |0      |1     |1        |
    | subtração       |9      |7     |2        |
    | multiplicação   |1      |3     |3        |
    | divisão         |8      |2     |4        |

