
Given(/^que eu acesso a Calculadora$/, () => {
	cy.visit('/');
});

Then(/^desejo realizar uma "([^"]*)"$/, (operacaoDesejada) => {
	
    let operador;

    switch (operacaoDesejada) {
        case 'soma':
            operador = '+'
            break;
        case 'subtração':
            operador = '-'
            break;
        case 'multiplicação':
            operador = 'x'
            break;
        case 'divisão':
            operador = '÷'
            break;
        default:
            break;
    }
    
    Cypress.env('operador', operador); //Nesse momento, estou salvando a operação que o usuário deseja realizar 
});

When(/^informar os valores "([^"]*)" e "([^"]*)"$/, (valor1,valor2) => {
    cy.get('div[class="button"], .button.zero').as('valores');
    cy.get('.operator').as('operadores');

    // informar valor 1
    // clicar na operacao desejada (soma)
    // informar o valor 2

    cy.get('@valores').contains(valor1).click()  //Estou pegando os elementos na variável valores, e pegando o valor específico que é passado na feature
    cy.get('@operadores').contains(`${Cypress.env('operador')}`).click() // Como no passo 2 armazenamos a operação do usuário, estamos pegando dentro dos elementos o operador que está na feature
    cy.get('@valores').contains(valor2).click() ///Estou pegando os elementos na variável valores, e pegando o valor específico que é passado na feature
    
});

When(/^finalizar a conta$/, () => {
	cy.get('@operadores').contains('=').click()
});

Then(/^devo obter o resultado "([^"]*)"$/, (resultadoEsperado) => {
	cy.get('.display').as('resultado')
    .invoke('text')//retorna o valor de texto de um elemento 
    .should('be.equal', resultadoEsperado)
    cy.screenshot();
});
